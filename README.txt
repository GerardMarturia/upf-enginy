Aquest fitxer conté la informació bàsica per compilar la petita simulació amb la base de dades inventada. 
Un cop descarregat el repositori, obriu el fitxer dencity.py amb qualsevol editor de codi i executecuteu-lo. 

Com podeu veure extreiem les dades de la base de dades database.csv i l'emmagatzem segons quin sigui la seva categoria. 
A partir d'aquest fem els càlculs necessaris de la densitat (tant amb el nombre de persones com amb l'estimació) 
i creem les condicions if amb els vandalls per a decidir en quin estat ens trobem. 

Finalment fem un print del resultat i dels mails que s'haurien de notificar. Cal esmentar que en l'aplicació desitjada tindriem les 
bases de dades de contacte i de càlcul de densitat per separat, ja que la primera hauria d'estar encriptada. 
