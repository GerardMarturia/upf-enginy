#Here we will do the small implementation of the code in Python

import csv
import numpy as np
import statistics

#GLOBAL variables change
locations = [["location","area"],["st joan 1",10],["st joan 2",10]] #m² of one place
thr_hn = 0.555 #people/m² threshold high to normal.
thr_nl = 0.075 #normal to low
density_thr = 0.05
#Calculate people
def calculate(place):
    ID = []
    mail = []
    people = []
    estimation = []
    estimation_v = []
    estimation2 = []
    estimation2_v = []
    location = []
    duration = []
    duration_v = []
    mails_v = []

    #Extracting the information
    with open("./data/database.csv", newline='') as f:
        reader = csv.reader(f) 
        for row in reader:
            ID.append(row[0])
            mail.append(row[1])
            people.append(row[2])
            estimation.append(row[3])
            estimation2.append(row[4])
            location.append(row[5])
            duration.append(row[6])
    print(place)
    for i in range(1,len(locations)):
        for name in locations[i]:
            #print("Iteration", name)
            if name == place: #condició que sigui aqesta location que diem
                area = locations[i][1]
            
    summ = 0 #people in the area
    for i in range(1,len(location)):
        if location[i] == place: #condició que sigui aqesta location que diem
            summ = summ + float(people[i]) 
            estimation_v.append(float(estimation[i]))
            estimation2_v.append(estimation2[i])
            duration_v.append(float(duration[i]))
            mails_v.append(mail[i])

    #Density alert from users and their colleagues
    print("Summ and area", summ, area)
    density = (summ/area)
    #Decision taking
    if (density >= thr_hn):
        print("Users have to be notified as density is %s people/m² considered HIGH" % density)
        print("The emails are the following",mails_v) #whatever way we want to communicate
    if (thr_nl <= density and density <= thr_hn):
        print("Users don't have to be notified as density is %s people/m² considered NORMAL" % density)
        #print("The emails are the following",mails_v) #whatever way we want to communicate
    if (density <= thr_nl):
        print("Users don't have to be notified as density is %s people/m² considered LOW" % density)
        #print("The emails are the following",mails_v) #whatever way we want to communicate
    
    #Density alert from estimation: trobar manera de fer-ho mb estimació Baixa/Normal/Alta
    est = ((statistics.mean(estimation_v))/area)
    #Hem de fer la mean de estimation en alta/normal/baixa
    dur = (statistics.mean(duration_v))
    
    #Decision taking with estimation
    if (est >= thr_hn):
        print("Users have to be notified as ESTIMATED density is %s people/m² considered HIGH" % est)
        print("The emails are the following",mails_v) #whatever way we want to communicate
    if (thr_nl <= est and est < thr_hn):
        print("Users don't have to be notified as ESTIMATED density is %s people/m² considered NORMAL" % est)
        #print("The emails are the following",mails_v) #whatever way we want to communicate
    if (est < thr_nl):
        print("Users don't have to be notified as ESTIMATED density is %s people/m² considered LOW" % est)
        #print("The emails are the following",mails_v) #whatever way we want to communicate
   
calculate("st joan 1")
calculate("st joan 2")



        

